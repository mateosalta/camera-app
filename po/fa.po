# Persian translation for camera-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the camera-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: camera-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-23 17:57+0000\n"
"PO-Revision-Date: 2022-03-10 09:21+0000\n"
"Last-Translator: Behzad <behzadasbahi@gmail.com>\n"
"Language-Team: Persian <https://translate.ubports.com/projects/ubports/"
"camera-app/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-12-01 04:53+0000\n"

#: ../AdvancedOptions.qml:16
msgid "Settings"
msgstr "تنظیمات"

#: ../AdvancedOptions.qml:48
msgid "Add date stamp on captured images"
msgstr "تمبر تاریخ را بر روی تصاویر ضبط شده اضافه کنید"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:70
msgid "Format"
msgstr "قالب بندی"

#: ../AdvancedOptions.qml:122
msgid "Date formatting keywords"
msgstr "کلمات کلیدی قالب بندی تاریخ"

#: ../AdvancedOptions.qml:127
msgid "the day as number without a leading zero (1 to 31)"
msgstr "روز به عنوان شماره بدون صفر پیشرو (1 تا 31)"

#: ../AdvancedOptions.qml:128
msgid "the day as number with a leading zero (01 to 31)"
msgstr "روز به عنوان عدد صفر پیشرو (01 تا 31)"

#: ../AdvancedOptions.qml:129
msgid "the abbreviated localized day name (e.g. 'Mon' to 'Sun')."
msgstr "نام مختصر روز محلی (به عنوان مثال \"دوشنبه\" تا \"یکشنبه\")."

#: ../AdvancedOptions.qml:130
msgid "the long localized day name (e.g. 'Monday' to 'Sunday')."
msgstr "نام روز محلی طولانی (به عنوان مثال \"دوشنبه\" تا \"یکشنبه\")."

#: ../AdvancedOptions.qml:131
msgid "the month as number without a leading zero (1 to 12)"
msgstr "ماه به عنوان شماره بدون صفر پیشرو (1 تا 12)"

#: ../AdvancedOptions.qml:132
msgid "the month as number with a leading zero (01 to 12)"
msgstr "ماه به عنوان شماره با صفر پیشرو (1 تا 12)"

#: ../AdvancedOptions.qml:133
msgid "the abbreviated localized month name (e.g. 'Jan' to 'Dec')."
msgstr "نام مختصر ماه محلی (به عنوان مثال \"ژانویه\" تا \"دسامبر\")."

#: ../AdvancedOptions.qml:134
msgid "the long localized month name (e.g. 'January' to 'December')."
msgstr "نام ماه محلی طولانی (به عنوان مثال \"ژانویه\" تا \"دسامبر\")."

#: ../AdvancedOptions.qml:135
msgid "the year as two digit number (00 to 99)"
msgstr "سال به عنوان شماره دو رقمی (00 تا 99)"

#: ../AdvancedOptions.qml:136
msgid ""
"the year as four digit number. If the year is negative, a minus sign is "
"prepended in addition."
msgstr ""
"سال به عنوان شماره چهار رقمی. اگر سال منفی باشد ، علائم منفی علاوه بر آن "
"اضافه می شود."

#: ../AdvancedOptions.qml:137
msgid "the hour without a leading zero (0 to 23 or 1 to 12 if AM/PM display)"
msgstr "ساعت بدون صفر پیشرو (0 تا 23 یا 1 تا 12 در صورت نمایش AM / PM)"

#: ../AdvancedOptions.qml:138
msgid "the hour with a leading zero (00 to 23 or 01 to 12 if AM/PM display)"
msgstr "ساعت با صفر پیشرو (د 00 تا 23 یا 01 تا 12 در صورت نمایش AM/PM)"

#: ../AdvancedOptions.qml:139
msgid "the hour without a leading zero (0 to 23, even with AM/PM display)"
msgstr "ساعت بدون صفر پیشرو (0 تا 23 ، حتی با نمایش AM / PM)"

#: ../AdvancedOptions.qml:140
msgid "the hour with a leading zero (00 to 23, even with AM/PM display)"
msgstr "ساعت با صفر پیشرو (00 تا 23 ، حتی با نمایش AM / PM)"

#: ../AdvancedOptions.qml:141
msgid "the minute without a leading zero (0 to 59)"
msgstr "دقیقه بدون صفر پیشرو (0 تا 59)"

#: ../AdvancedOptions.qml:142
msgid "the minute with a leading zero (00 to 59)"
msgstr "دقیقه با صفر پیشرو (00 تا 59)"

#: ../AdvancedOptions.qml:143
msgid "the second without a leading zero (0 to 59)"
msgstr "ثانیه بدون صفر پیشرو (0 تا 59)"

#: ../AdvancedOptions.qml:144
msgid "the second with a leading zero (00 to 59)"
msgstr "ثانیه با صفر پیشرو (00 تا 59)"

#: ../AdvancedOptions.qml:145
msgid "the milliseconds without leading zeroes (0 to 999)"
msgstr "میلی ثانیه بدون صفرهای پیشرو (0 تا 999)"

#: ../AdvancedOptions.qml:146
msgid "the milliseconds with leading zeroes (000 to 999)"
msgstr "میلی ثانیه با صفرهای پیشرو (000 تا 999)"

#: ../AdvancedOptions.qml:147
msgid "use AM/PM display. AP will be replaced by either 'AM' or 'PM'."
msgstr "از نمایشگر AM / PM استفاده کنید. AP با «AM» یا «PM» جایگزین خواهد شد."

#: ../AdvancedOptions.qml:148
msgid "use am/pm display. ap will be replaced by either 'am' or 'pm'."
msgstr ""
"از نمایشگر am / pm استفاده کنید. ap با \"am\" یا \"pm\" جایگزین خواهد شد."

#: ../AdvancedOptions.qml:149
msgid "the timezone (for example 'CEST')"
msgstr "منطقه زمانی (به عنوان مثال \"CEST\")"

#: ../AdvancedOptions.qml:158
msgid "Add to Format"
msgstr "به قالب اضافه کنید"

#. TRANSLATORS: this refers to the color of date stamp added to captured images
#: ../AdvancedOptions.qml:189
msgid "Color"
msgstr "رنگ"

#. TRANSLATORS: this refers to the alignment of date stamp within captured images (bottom left, top right,etc..)
#: ../AdvancedOptions.qml:257
msgid "Alignment"
msgstr "هم ترازی"

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:303
msgid "Opacity"
msgstr "سطح کدورت"

#: ../DeleteDialog.qml:24
msgid "Delete media?"
msgstr "حذف رسانه؟"

#: ../DeleteDialog.qml:34 ../PhotogridView.qml:56 ../SlideshowView.qml:75
msgid "Delete"
msgstr "حذف"

#: ../DeleteDialog.qml:40 ../ViewFinderOverlay.qml:1122
#: ../ViewFinderOverlay.qml:1136 ../ViewFinderOverlay.qml:1175
#: ../ViewFinderView.qml:494
msgid "Cancel"
msgstr "لغو"

#: ../GalleryView.qml:200
msgid "No media available."
msgstr "هیچ رسانه‌ای موجود نیست."

#: ../GalleryView.qml:235
msgid "Scanning for content..."
msgstr "در حال پویش برای محتوا…"

#: ../GalleryViewHeader.qml:82 ../SlideshowView.qml:45
msgid "Select"
msgstr "گزینش"

#: ../GalleryViewHeader.qml:83
msgid "Edit Photo"
msgstr "ویرایش عکس"

#: ../GalleryViewHeader.qml:83
msgid "Photo Roll"
msgstr "چرخش عکس"

#: ../Information.qml:20
msgid "About"
msgstr "درباره"

#: ../Information.qml:82
msgid "Get the source"
msgstr "بدست آوردن منبع"

#: ../Information.qml:83
msgid "Report issues"
msgstr "گزارش مشکلات"

#: ../Information.qml:84
msgid "Help translate"
msgstr "کمک به ترجمه"

#: ../MediaInfoPopover.qml:25
msgid "Media Information"
msgstr "اطلاعات رسانه ای"

#: ../MediaInfoPopover.qml:30
#, qt-format
msgid "Name : %1"
msgstr "نام : %1"

#: ../MediaInfoPopover.qml:33
#, qt-format
msgid "Type : %1"
msgstr "نوع : %1"

#: ../MediaInfoPopover.qml:37
#, qt-format
msgid "Width : %1"
msgstr "عرض : %1"

#: ../MediaInfoPopover.qml:41
#, qt-format
msgid "Height : %1"
msgstr "ارتفاع : %1"

#: ../NoSpaceHint.qml:33
msgid "No space left on device, free up space to continue."
msgstr "هیچ فضایی روی دستگاه نمانده است. برای ادامه مقداری فضا خالی کنید."

#: ../PhotoRollHint.qml:69
msgid "Swipe left for photo roll"
msgstr "برای طومار عکس به چپ بکشید"

#: ../PhotogridView.qml:42 ../SlideshowView.qml:60
msgid "Share"
msgstr "به‌اشتراک گذاری"

#: ../PhotogridView.qml:69 ../SlideshowView.qml:53
msgid "Gallery"
msgstr "گالری"

#: ../SlideshowView.qml:68
msgid "Image Info"
msgstr "اطلاعات تصویر"

#: ../SlideshowView.qml:86
msgid "Edit"
msgstr "ویرایش"

#: ../SlideshowView.qml:441
msgid "Back to Photo roll"
msgstr "بازگشت به رول عکس"

#: ../UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "اشتراک گذاری امکان پذیر نیست"

#: ../UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "به اشتراک گذاشتن عکس ها و فیلم ها به طور همزمان امکان پذیر نیست"

#: ../UnableShareDialog.qml:30
msgid "Ok"
msgstr "قبول"

#: ../ViewFinderOverlay.qml:367 ../ViewFinderOverlay.qml:390
#: ../ViewFinderOverlay.qml:418 ../ViewFinderOverlay.qml:441
#: ../ViewFinderOverlay.qml:526 ../ViewFinderOverlay.qml:589
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:42
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:65
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:86
msgid "On"
msgstr "روشن"

#: ../ViewFinderOverlay.qml:372 ../ViewFinderOverlay.qml:400
#: ../ViewFinderOverlay.qml:423 ../ViewFinderOverlay.qml:446
#: ../ViewFinderOverlay.qml:465 ../ViewFinderOverlay.qml:531
#: ../ViewFinderOverlay.qml:599
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:47
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:70
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:91
msgid "Off"
msgstr "خاموش"

#: ../ViewFinderOverlay.qml:395
msgid "Auto"
msgstr "خودکار"

#: ../ViewFinderOverlay.qml:432
msgid "HDR"
msgstr "HDR"

#: ../ViewFinderOverlay.qml:470
msgid "5 seconds"
msgstr "۵ ثانیه"

#: ../ViewFinderOverlay.qml:475
msgid "15 seconds"
msgstr "۱۵ ثانیه"

#: ../ViewFinderOverlay.qml:493
msgid "Fine Quality"
msgstr "کیفیت خوب"

#: ../ViewFinderOverlay.qml:498
msgid "High Quality"
msgstr "کیفیت بالا"

#: ../ViewFinderOverlay.qml:503
msgid "Normal Quality"
msgstr "کیفیت عادی"

#: ../ViewFinderOverlay.qml:508
msgid "Basic Quality"
msgstr "کیفیت پایه"

#. TRANSLATORS: this will be displayed on an small button so for it to fit it should be less then 3 characters long.
#: ../ViewFinderOverlay.qml:541
msgid "SD"
msgstr "اس‌دی"

#: ../ViewFinderOverlay.qml:549
msgid "Save to SD Card"
msgstr "دخیره در کارت اس‌دی"

#: ../ViewFinderOverlay.qml:554
msgid "Save internally"
msgstr "دخیره‌ی داخلی"

#: ../ViewFinderOverlay.qml:594
msgid "Vibrate"
msgstr "لرزش"

#: ../ViewFinderOverlay.qml:1119
msgid "Low storage space"
msgstr "فضای ذخیره‌ی کم"

#: ../ViewFinderOverlay.qml:1120
msgid ""
"You are running out of storage space. To continue without interruptions, "
"free up storage space now."
msgstr ""
"فضای ذخیره‌ی شما پر شده است. برای ادامه دادن بدون وقفه، هم‌اکنون مقداری فضا "
"خالی کنید."

#: ../ViewFinderOverlay.qml:1133
msgid "External storage not writeable"
msgstr "ذخیره سازی خارجی قابل نوشتن نیست"

#: ../ViewFinderOverlay.qml:1134
msgid ""
"It does not seem possible to write to your external storage media. Trying to "
"eject and insert it again might solve the issue, or you might need to format "
"it."
msgstr ""
"بنظر نمی رسد که بتوانید در رسانه های ذخیره سازی خارجی خود بنویسید. تلاش برای "
"بیرون کشیدن و درج دوباره آن ممکن است مسئله را برطرف کند ، یا ممکن است نیاز "
"به قالب بندی آن داشته باشید."

#: ../ViewFinderOverlay.qml:1172
msgid "Cannot access camera"
msgstr "نمی‌توان به دوربین دسترسی داشت"

#: ../ViewFinderOverlay.qml:1173
msgid ""
"Camera app doesn't have permission to access the camera hardware or another "
"error occurred.\n"
"\n"
"If granting permission does not resolve this problem, reboot your device."
msgstr ""
"برنامه دوربین اجازهٔ دسترسی به سخت‌افزار دوربین را نداشته یا خطایی دیگر رخ "
"داده است.\n"
"\n"
"\n"
"اگر اعطای دسترسی این مشکل را حل نکرد، تلفنتان را راه‌انداری دوباره کنید."

#: ../ViewFinderOverlay.qml:1182
msgid "Edit Permissions"
msgstr "ویرایش مجوّزها"

#: ../ViewFinderView.qml:485
msgid "Capture failed"
msgstr "ضبط انجام نشد"

#: ../ViewFinderView.qml:490
msgid ""
"Replacing your external media, formatting it, or restarting the device might "
"fix the problem."
msgstr ""
"جایگزینی رسانه خارجی ، قالب بندی آن یا راه اندازی مجدد دستگاه ممکن است مشکل "
"را برطرف کند."

#: ../ViewFinderView.qml:491
msgid "Restarting your device might fix the problem."
msgstr "راه اندازی مجدد دستگاه ممکن است این مشکل را برطرف کند."

#: ../camera-app.qml:57
msgid "Flash"
msgstr "فلاش"

#: ../camera-app.qml:58
msgid "Light;Dark"
msgstr "روشن;تیره"

#: ../camera-app.qml:61
msgid "Flip Camera"
msgstr "برعکس‌کردن دوربین"

#: ../camera-app.qml:62
msgid "Front Facing;Back Facing"
msgstr "رو به جلو;رو به پشت"

#: ../camera-app.qml:65
msgid "Shutter"
msgstr "شاتر"

#: ../camera-app.qml:66
msgid "Take a Photo;Snap;Record"
msgstr "گرفتن عکس;گرفتن;ضبط"

#: ../camera-app.qml:69
msgid "Mode"
msgstr "حالت‌"

#: ../camera-app.qml:70
msgid "Stills;Video"
msgstr "ثابت‌ها;ویدیو"

#: ../camera-app.qml:74
msgid "White Balance"
msgstr "توازن نور سفید"

#: ../camera-app.qml:75
msgid "Lighting Condition;Day;Cloudy;Inside"
msgstr "شرایط نوری;روز;ابری;داخلی"

#: ../camera-app.qml:380
#, qt-format
msgid "<b>%1</b> photos taken today"
msgstr "امروز <b>%1</b> عکس گرفته شد"

#: ../camera-app.qml:381
msgid "No photos taken today"
msgstr "امروز هیچ عکسی گرفته نشد"

#: ../camera-app.qml:391
#, qt-format
msgid "<b>%1</b> videos recorded today"
msgstr "امروز <b>%1</b> ویدیو ضبط شد"

#: ../camera-app.qml:392
msgid "No videos recorded today"
msgstr "امروز هیچ ویدیویی ضبط نشد"

#: camera-app.desktop.in.in.h:1
msgid "Camera"
msgstr "دوربین"

#: camera-app.desktop.in.in.h:2
msgid "Camera application"
msgstr "برنامه‌ی دوربین"

#: camera-app.desktop.in.in.h:3
msgid "Photos;Videos;Capture;Shoot;Snapshot;Record"
msgstr "عکس‌ها;ویدیوها;ضبط;ثبت;عکّاسی;فیلم‌برداری"
